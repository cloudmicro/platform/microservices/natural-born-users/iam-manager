package util

import (

	migration "gitlab.com/cloudmicro/platform/microservices/natural-born-users/migrations.git/pkg"
)

//SetUpCustomer ...
func SetUpCustomer(action string, schema string) bool {

	return migration.Migrate(action, schema)
}
